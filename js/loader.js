/*global jQuery, Highcharts, OWM */

(function ($, OWM) {

    "use strict";
    var API_KEY = "cb1532243aa49da13e06870ca964de70";
    var POSITION = "-0.180653,-78.467834";
    /**
     * @param {string|number} name
     * @param {string} [url]
     */
    var urlParam = function (name, url) {
        if (!url) {
            url = window.location.href;
        }
        if (typeof name == "string") {
            var results = new RegExp('[\\#&?]!?' + name + '=([^&#]*)').exec(url);
            if (!results) {
                return undefined;
            }
            return results[1] || undefined;
        }
        else if (typeof name == "number") {
            var regex = /[\\#&?]!?([^&]*)/g;
            var i = 0;
            while ((results = regex.exec(url)) !== null) {
                if (i == name) {
                    return results[1] || undefined;
                }
                i++;
            }
            return undefined;
        }
    };

    /**
     * @param {object} d
     */
    var ISODateString = function (d) {
        function pad(n) {
            return (n < 10) ? '0' + n : n;
        }

        return d.getFullYear() + '-' +
            pad(d.getMonth() + 1) + '-' +
            pad(d.getDate()) + 'T' +
            pad(d.getHours()) + ':' +
            pad(d.getMinutes()) + ':' +
            pad(d.getSeconds());
    };

    /**
     * @param {string} id
     */
    var hide = function (id) {
        $('#' + id).hide();
    };

    /**
     * @param {string} id
     */
    var show = function (id) {
        $('#' + id).show();
    };

    /**
     * @param {object} e
     */
    var errorHandler = function (e) {
        console.log(e.status + ' ' + e.statusText);
    };

    var dataReceived = false;

    var date = new Date();

    date.setHours(0);
    date.setMilliseconds(0);
    date.setMinutes(0);
    date.setSeconds(0);

    var minus1 = ISODateString(date);

    date.setDate(date.getDate() - 1);

    var minus2 = ISODateString(date);

    /**
     * @param {object} minus1
     * @param {object} minus2
     * @param {object} forecast
     */
    var mergeData = function (minus1, minus2, forecast, position) {

        dataReceived = true;

        var hourlyspan = [];
        var dailyspan = [];
        var current = forecast.currently;

        for (var i = 1; i < minus2.hourly.data.length; i++) {
            if (minus2.hourly.data[i].time < minus1.hourly.data[0].time) {
                hourlyspan.push(minus2.hourly.data[i]);
            }
        }

        for (i = 0; i < minus1.hourly.data.length; i++) {
            if (minus1.hourly.data[i].time < forecast.hourly.data[0].time) {
                hourlyspan.push(minus1.hourly.data[i]);
            }
        }

        for (i = 0; i < forecast.hourly.data.length; i++) {
            hourlyspan.push(forecast.hourly.data[i]);
        }

        for (i = 0; i < minus2.daily.data.length; i++) {
            if (minus2.daily.data[i].time < minus1.daily.data[0].time) {
                dailyspan.push(minus2.daily.data[i]);
            }
        }

        for (i = 0; i < minus1.daily.data.length; i++) {
            if (minus1.daily.data[i].time < forecast.daily.data[0].time) {
                dailyspan.push(minus1.daily.data[i]);
            }
        }

        for (i = 0; i < forecast.daily.data.length; i++) {
            dailyspan.push(forecast.daily.data[i]);
        }

        var position = position.split(",").map(parseFloat);

        renderData(hourlyspan, forecast.hourly.data, current, dailyspan, position);
        window.localStorage.hourlyspan = hourlyspan;
        window.localStorage.hourly = forecast.hourly.data;
        window.localStorage.current = current;
        window.localStorage.dailyspan = dailyspan;

    };

    /**
     * @param {object} span
     * @param {array} future
     * @param {array} current
     * @param {array} daily
     */
    var renderData = function (span, future, current, daily, position) {

        hide('loading');
        show('map-rain');
        show('map-lightning');
        show('suntimes');

        var owm = new OWM();

        owm.dialTemperature('dial-temperature', current);
        owm.placeIcon('icon-weather', current);
        owm.placePin('pin', position[0], position[1]);
        owm.placeMap('map-image', position[0], position[1]);

        owm.dialWind('dial-wind', future);
        owm.plotPressure('chart-pressure', span, current, daily);
        owm.plotTemperature('chart-temp', span, daily);
        //owm.plotHumidity('chart-humidity', span, daily);
        owm.plotWindSpeed('chart-wind', span, daily);
        owm.plotRain('chart-rain', span, daily);
        owm.placeDate('date', span);
        owm.placeSuntimes('suntimes', daily);

    };


    /**
     * @param {string} apikey
     * @param {string} position
     */
    var loadData = function (apikey, position, minus1, minus2) {
        $.when(
            $.ajax({
                url: "https://api.darksky.net/forecast/" + apikey + "/" + position + "," + minus1 + "?extend=hourly&units=si&callback=?",
                type: 'GET',
                dataType: 'jsonp',
                jsonpCallback: 'past2callback',
                error: errorHandler,
                cache: true
            }),
            $.ajax({
                url: "https://api.darksky.net/forecast/" + apikey + "/" + position + "," + minus2 + "?extend=hourly&units=si&callback=?",
                type: 'GET',
                dataType: 'jsonp',
                jsonpCallback: 'past1callback',
                error: errorHandler,
                cache: true
            }),
            $.ajax({
                url: "https://api.darksky.net/forecast/" + apikey + "/" + position + "?extend=hourly&units=si&callback=?",
                type: 'GET',
                dataType: 'jsonp',
                jsonpCallback: 'futurecallback',
                error: errorHandler,
                cache: true
            })).done(function (a1, a2, a3) {
            mergeData(a1[0], a2[0], a3[0], position);
        }).fail(function () {
            location.reload(true);
        });
    };

    var binding_download_btns = function () {


        $('#download-plotPressure').on('click', function (e) {
            e.preventDefault();
            var data = JSON.parse(localStorage.plotPressure);
            var header = ["fecha", "plotPressure"];
            data.splice(0, 0, header);
            download_data(data, 'plotPressure');
        });
        $('#download-plotRain').on('click', function (e) {
            e.preventDefault();
            var data = JSON.parse(localStorage.plotRain);
            var header = ["fecha", "plotRain"];
            data.splice(0, 0, header);
            download_data(data, 'plotRain');
        });
        $('#download-plotHumidity').on('click', function (e) {
            e.preventDefault();
            var data = JSON.parse(localStorage.plotHumidity);
            var header = ["fecha", "plotHumidity"];
            data.splice(0, 0, header);
            download_data(data, 'plotHumidity');
        });
        $('#download-plotTemperature').on('click', function (e) {
            e.preventDefault();
            var data = JSON.parse(localStorage.plotTemperature);
            var header = ["fecha", "plotTemperature"];
            data.splice(0, 0, header);
            download_data(data, 'plotTemperature');

        });
        $('#download-plotWindSpeed').on('click', function (e) {
            e.preventDefault();
            var data = JSON.parse(localStorage.plotWindSpeed);
            var header = ["fecha", "plotWindSpeed"];
            data.splice(0, 0, header);
            download_data(data, 'plotWindSpeed');

        });
    };

    var download_data = function (data, title) {

        JSONToCSVConvertor(data, title, true);
    };

    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
        //var arrData = JSONData;
        var CSV = '';
        //Set Report title in first row or line

        //CSV += ReportTitle + '\r\n\n';

        //This condition will generate the Label/Header
        if (ShowLabel) {
            var row = "";

            //This loop will extract the label from 1st index of on array
            for (var index in arrData[0]) {

                //Now convert each value to string and comma-seprated
                row += arrData[0][index] + ',';
            }

            row = row.slice(0, -1);

            //append Label row with line break
            CSV += row + '\r\n';
        }

        //1st loop is to extract each row

        for (var i = ShowLabel ? 1 : 0; i < arrData.length; i++) {
            var row = "";

            //2nd loop will extract each column and convert it in string comma-seprated
            for (var index in arrData[i]) {
                if (index == 0) {
                    arrData[i][index] = moment(arrData[i][index]).format('YYYY-MM-DD hh:mm:ss');
                }
                row += '"' + arrData[i][index] + '",';
            }

            row = row.slice(0, row.length - 1);

            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV == '') {
            alert("Invalid data");
            return;
        }

        //Generate a file name
        var fileName = "ClimaUCE_";
        //this will remove the blank-spaces from the title and replace it with an underscore
        fileName += ReportTitle.replace(/ /g, "_");

        //Initialize file format you want csv or xls
        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension

        //this trick will generate a temp <a /> tag
        var link = document.createElement("a");
        link.href = uri;

        //set the visibility hidden so it will not effect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";

        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }


    $(function () {

        var position;
        var apikey;

        setTimeout(function () {
            if (!dataReceived) {
                location.reload(true);
            }
        }, 60000);
        apikey = urlParam('apikey') || API_KEY;

        if (apikey) {
            if (urlParam('position')) {
                loadData(apikey, urlParam('position'), minus1, minus2)
            }
            /*else if (navigator.geolocation) {
             navigator.geolocation.getCurrentPosition(
             function (position) {
             var latitude = Math.round(position.coords.latitude * 1000) / 1000
             var longitude = Math.round(position.coords.longitude * 1000) / 1000
             loadData(apikey, latitude.toString() + "," + longitude.toString(), minus1, minus2);
             },
             function () {
             loadData(apikey, POSITION, minus1, minus2)
             });
             }*/
            else {
                loadData(apikey, POSITION, minus1, minus2)
            }
        } else {
            show("apimissing");
        }

        binding_download_btns();


    });

}(jQuery, OWM));
